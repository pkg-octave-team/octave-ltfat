1;

# List of tests to exclude
global LTFAT_TEST_EXCLUDE
LTFAT_TEST_EXCLUDE = {};

# Ensure reproducibility (see Bug#1054178)
rand ("seed", 1234)

disp ("[running test_all_ltfat]");
test_all_ltfat ('double');
